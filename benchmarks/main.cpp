#include <vector>
#include <list>
#include <iostream>
#include <unistd.h>

using namespace std;

uint64_t sum_of_vector_squared(int num_entries)
{
    vector<uint32_t> num_vector(num_entries);
    vector<uint64_t> squared_vector(num_entries);
    uint64_t sum = 0;
    for (unsigned int index = 0; index < num_entries; ++index)
    {
        /* initialize random seed: */
        srand(time(NULL));
        num_vector[index] = rand();
    }
    for (unsigned int index = 0; index < num_entries; ++index)
    {
        squared_vector[index] = num_vector[index] ^ 2;
    }
    for (unsigned int index = 0; index < num_entries; ++index)
    {
        sum += squared_vector[index];
    }
    return sum;
}

uint64_t sum_of_list_squared(int num_entries)
{
    list<uint32_t> num_list(num_entries);
    list<uint64_t> squared_list(num_entries);
    uint64_t sum = 0;
    for (unsigned int index = 0; index < num_entries; ++index)
    {
        /* initialize random seed: */
        srand(time(NULL));
        num_list.push_back(rand());
        squared_list.push_back(num_list.back() ^ 2);
        sum += squared_list.back();
    }
    return sum;
}
