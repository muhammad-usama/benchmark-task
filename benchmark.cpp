#include <benchmark/benchmark.h>
#include "benchmarks/main.cpp"

static void BM_Vector(benchmark::State &state)
{
    for (auto _ : state)
        auto sum = sum_of_vector_squared(state.range(0));
}

BENCHMARK(BM_Vector)->Range(1, 100000);

static void BM_List(benchmark::State &state)
{
    for (auto _ : state)
        auto sum = sum_of_list_squared(state.range(0));
}

BENCHMARK(BM_List)->Range(1, 100000);

BENCHMARK_MAIN();